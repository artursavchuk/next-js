import withPlugins from 'next-compose-plugins'
import createNextIntlPlugin from 'next-intl/plugin'
import withSvgr from 'next-plugin-svgr'

const withNextIntl = createNextIntlPlugin()

export default withPlugins([withSvgr, withNextIntl])
