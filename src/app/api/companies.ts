'use server'

import { getToken } from '@/helpers/auth'
import { BaseResponse, ErrorResponse, ResponseData } from '@/types/api'
import {
  Company,
  CompanyRequest,
  UpdateCompanyRequest,
} from '@/types/api/companies'
import { revalidateTag } from 'next/cache'

export const getCompaniesList = async () => {
  const token = getToken()
  if (!token) return null

  const response = await fetch(process.env.NEXT_PUBLIC_API_URL + 'companies', {
    method: 'GET',
    headers: { Authorization: `Bearer ${token}` },
    next: { tags: ['getCompaniesList'] },
  })

  const json = (await response.json()) as ResponseData<Company[]>
  return json.data
}

export const createCompany = async (fv: CompanyRequest) => {
  const token = getToken()
  if (!token) return null

  const response = await fetch(process.env.NEXT_PUBLIC_API_URL + 'companies', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;',
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(fv),
  })

  const json = (await response.json()) as ResponseData<Company> | ErrorResponse

  if ('success' in json) {
    return json
  }

  revalidateTag('getCompaniesList')
  return json.data
}

export const getCompany = async (id: number) => {
  const token = getToken()
  if (!token) return null

  const response = await fetch(
    process.env.NEXT_PUBLIC_API_URL + `companies/${id}`,
    {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
      next: { tags: ['getCompany'] },
    }
  )

  const json = (await response.json()) as ResponseData<Company>
  return json.data
}

export const updateCompany = async ({ id, fv }: UpdateCompanyRequest) => {
  const token = getToken()
  if (!token) return null

  const response = await fetch(
    process.env.NEXT_PUBLIC_API_URL + `companies/${id}`,
    {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json;',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(fv),
    }
  )

  const json = (await response.json()) as ResponseData<Company> | ErrorResponse

  if ('success' in json) {
    return json
  }

  revalidateTag('getCompaniesList')
  revalidateTag('getCompany')
  return json.data
}

export const deleteCompany = async (id: number) => {
  const token = getToken()
  if (!token) return null

  const response = await fetch(
    process.env.NEXT_PUBLIC_API_URL + `companies/${id}`,
    {
      method: 'DELETE',
      headers: { Authorization: `Bearer ${token}` },
    }
  )

  revalidateTag('getCompaniesList')
  return (await response.json()) as BaseResponse<[]>
}
