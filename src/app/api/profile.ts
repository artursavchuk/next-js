'use server'

import { getToken } from '@/helpers/auth'
import { ErrorResponse, ResponseData } from '@/types/api'
import { Profile } from '@/types/api/profile'
import { revalidateTag } from 'next/cache'
import { cookies } from 'next/headers'

export const getProfile = async () => {
  const token = getToken()
  if (!token) return null

  const response = await fetch(process.env.NEXT_PUBLIC_API_URL + 'profile', {
    method: 'GET',
    headers: { Authorization: `Bearer ${token}` },
    next: { tags: ['getProfile'] },
  })

  const json = (await response.json()) as ResponseData<Profile>
  return json.data
}

export const updateProfile = async (fd: FormData) => {
  const token = getToken()
  if (!token) return null

  const response = await fetch(process.env.NEXT_PUBLIC_API_URL + 'profile', {
    method: 'POST',
    headers: { Authorization: `Bearer ${token}` },
    body: fd,
  })

  const json = (await response.json()) as ResponseData<Profile> | ErrorResponse

  if ('success' in json) {
    return json
  }

  revalidateTag('getProfile')
  return json.data
}
