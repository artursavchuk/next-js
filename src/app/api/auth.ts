'use server'

import { FormValues as RegistrationFV } from '@/components/ui/RegistrationForm'
import { FormValues as LoginFV } from '@/components/ui/LoginForm'
import { BaseResponse } from '@/types/api'
import { LoginResponse } from '@/types/api/login'
import { cookies } from 'next/headers'
import { getToken } from '@/helpers/auth'

export const registration = async (fv: RegistrationFV) => {
  const response = await fetch(process.env.NEXT_PUBLIC_API_URL + 'register', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json;' },
    body: JSON.stringify(fv),
  })

  return (await response.json()) as BaseResponse<[]>
}

export const login = async (fv: LoginFV) => {
  const cookiesStore = cookies()
  const month = 60 * 60 * 24 * 30

  const response = await fetch(process.env.NEXT_PUBLIC_API_URL + 'login', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json;' },
    body: JSON.stringify(fv),
  })

  const json = (await response.json()) as BaseResponse<LoginResponse>

  if (json.success) {
    cookiesStore.set('t', json.data.access_token, { maxAge: month })
  }

  return json
}

export const logout = async () => {
  const cookiesStore = cookies()
  const token = getToken()
  if (!token) return null

  const response = await fetch(process.env.NEXT_PUBLIC_API_URL + 'logout', {
    method: 'POST',
    headers: { Authorization: `Bearer ${token}` },
  })

  const json = (await response.json()) as BaseResponse<[]>

  if (json.success) {
    cookiesStore.delete('t')
  }

  return json
}
