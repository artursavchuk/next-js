'use server'

import { getToken } from '@/helpers/auth'
import { BaseResponse } from '@/types/api'
import { ImportRequest, ImportResponse } from '@/types/api/import'

export const importLeads = async ({ boardId, fd }: ImportRequest) => {
  const token = getToken()
  if (!token) return null

  const response = await fetch(
    process.env.NEXT_PUBLIC_API_URL + `boards/${boardId}/leads/import-file`,
    {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
      },
      body: fd,
    }
  )

  return (await response.json()) as BaseResponse<ImportResponse>
}
