'use server'

import qs from 'qs'
import { getToken } from '@/helpers/auth'
import { Lead, LeadsRequest, LeadsResponse } from '@/types/api/leads'
import { revalidateTag } from 'next/cache'
import { ResponseData } from '@/types/api'
import { redirect } from '@/navigation'
import { ROUTES } from '@/libs/router'

export const getLeads = async <T extends object>(args: LeadsRequest<T>) => {
  const { companyId, searchParams } = args
  const token = getToken()
  if (!token) return null

  const queryString = qs.stringify(searchParams, { skipNulls: true })
  const requestUrl =
    process.env.NEXT_PUBLIC_API_URL +
    `companies/${companyId}/unassigned-leads` +
    (queryString ? `?${queryString}` : '')

  const response = await fetch(requestUrl, {
    method: 'GET',
    headers: { Authorization: `Bearer ${token}` },
    next: { tags: ['getLeads'] },
  })

  const data: LeadsResponse = await response.json()
  return data
}

export const createLead = async (companyId: number, fd: FormData) => {
  const token = getToken()
  if (!token) return null

  const boardId = fd.get('board_id')
  const response = await fetch(
    process.env.NEXT_PUBLIC_API_URL + `boards/${boardId}/leads`,
    {
      method: 'POST',
      headers: { Authorization: `Bearer ${token}` },
      body: fd,
    }
  )

  const json: ResponseData<Lead> = await response.json()
  if (json.data) {
    redirect(ROUTES.LEADS + `?companyId=${companyId}`)
    revalidateTag('getLeads')
  }

  return json
}
