import './globals.css'
import clsx from 'clsx'
import { Inter } from 'next/font/google'
import { ILayoutProps } from '@/types/layout'
import { Providers } from './providers'
import { Toaster } from 'sonner'
import { Metadata } from 'next'
import { getTranslations } from 'next-intl/server'
import { Header } from '@/components/sections/Header'
import { Footer } from '@/components/sections/Footer'

const inter = Inter({ subsets: ['latin'] })

export const generateMetadata = async (): Promise<Metadata> => {
  const t = await getTranslations()
  return {
    title: t('metadata.common.title'),
    description: t('metadata.common.desc'),
  }
}

export default async function GlobalLayout({ children, params }: ILayoutProps) {
  return (
    <html lang={params.locale} className="scroll-smooth">
      <body
        className={clsx(
          inter.className,
          'min-h-[100dvh] flex flex-col overscroll-y-none'
        )}
      >
        <Toaster position="top-right" richColors closeButton />
        <Providers>
          <Header />
          <main className="grow h-full flex flex-col">
            {children}
          </main>
          <Footer />
        </Providers>
      </body>
    </html>
  )
}
