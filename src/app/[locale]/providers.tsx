'use client'

import { setupWebSocket } from '@/socket'
import { NextUIProvider } from '@nextui-org/react'
import { ReactNode, useEffect } from 'react'

type Props = {
  children: ReactNode
}

export const Providers = ({ children }: Props) => {
  useEffect(() => {
    setupWebSocket()
  }, [])

  return <NextUIProvider>{children}</NextUIProvider>
}
