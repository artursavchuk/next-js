import { Faq } from '@/components/sections/Faq'
import { Hero } from '@/components/sections/Hero'
import { Research } from '@/components/sections/Research'
import { Services } from '@/components/sections/Services'

export default function Page() {
  return (
    <>
      <Hero />
      <Services />
      <Research />
      <Faq />
    </>
  )
}
