import IconSpinner from '/public/spinner.svg'

export default function Loading() {
  return (
    <div className="p-10 h-[100vh] flex items-center justify-center">
      <IconSpinner className="w-h-9 inline-block h-9 animate-spin text-primary-400" />
    </div>
  )
}
