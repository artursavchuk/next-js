import { LoginForm, Translations } from '@/components/ui/LoginForm'
import { useValidation } from '@/hooks/useValidation'
import { useTranslations } from 'next-intl'
import { Metadata } from 'next'
import { getTranslations } from 'next-intl/server'

export const generateMetadata = async (): Promise<Metadata> => {
  const t = await getTranslations()
  return {
    title: t('metadata.sign_in.title'),
  }
}

export default function Page() {
  const t = useTranslations()
  const { rules } = useValidation()

  const translations: Translations = {
    labels: {
      email: t('form.email'),
      password: t('form.password'),
    },
    validations: {
      required: t('errors.required'),
      email: t('errors.email'),
      passwordMinLength: `${t('errors.min')} ${rules.passwordMinLength}`,
      passwordMaxLength: `${t('errors.max')} ${rules.passwordMaxLength}`,
    },
    submit: t('buttons.sign_in'),
  }

  return (
    <section>
      <h1 className="section-title text-center">{t('section_login.title')}</h1>
      <LoginForm t={translations} />
    </section>
  )
}
