import { ILayoutProps } from '@/types/layout'
import { Metadata } from 'next'
import { getTranslations } from 'next-intl/server'

export const generateMetadata = async (): Promise<Metadata> => {
  const t = await getTranslations()
  return {
    title: {
      template: t('metadata.auth.title'),
      default: '',
    },
    description: t('metadata.auth.desc'),
  }
}

export default function AuthLayout({ children, params }: ILayoutProps) {
  return (
    <div className="grow h-full section-py">
      <div className="container">
        <div className="max-w-[500px] mx-auto p-2">{children}</div>
      </div>
    </div>
  )
}
