import {
  RegistrationForm,
  Translations,
} from '@/components/ui/RegistrationForm'
import { useValidation } from '@/hooks/useValidation'
import { Metadata } from 'next'
import { useTranslations } from 'next-intl'
import { getTranslations } from 'next-intl/server'

export const generateMetadata = async (): Promise<Metadata> => {
  const t = await getTranslations()
  return {
    title: t('metadata.sign_up.title'),
  }
}

export default function Page() {
  const t = useTranslations()
  const { rules } = useValidation()

  const translations: Translations = {
    labels: {
      name: t('form.name'),
      email: t('form.email'),
      password: t('form.password'),
      confirmation: t('form.confirmation'),
    },
    validations: {
      required: t('errors.required'),
      email: t('errors.email'),
      nameMinLength: `${t('errors.min')} ${rules.nameMinLength}`,
      passwordMinLength: `${t('errors.min')} ${rules.passwordMinLength}`,
      passwordMaxLength: `${t('errors.max')} ${rules.passwordMaxLength}`,
      samePasswords: t('errors.samePasswords'),
    },
    submit: t('buttons.sign_up'),
  }

  return (
    <section>
      <h1 className="section-title text-center">
        {t('section_registration.title')}
      </h1>
      <RegistrationForm t={translations} />
    </section>
  )
}
