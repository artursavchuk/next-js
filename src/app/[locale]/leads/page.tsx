import { getCompaniesList } from '@/app/api/companies'
import { getLeads } from '@/app/api/leads'
import { Table } from '@/components/ui/Table'
import { Search } from '@/components/ui/Search'
import { QueryNames } from '@/hooks/useTable'
import { CompanySwitcher } from '@/components/ui/CompanySwitcher'
import { ROUTES } from '@/libs/router'
import { LinkButton } from '@/components/ui/LinkButton'

type Props = {
  searchParams: Record<Partial<QueryNames>, string>
}

export default async function Page({ searchParams }: Props) {
  const companies = (await getCompaniesList()) ?? []

  if (companies.length === 0) {
    return (
      <section className="section-py min-h-[50vh]">
        <div className="container flex flex-col">
          <h2 className="section-title text-center">Unassigned leads</h2>
          <div>Create first company to start</div>
        </div>
      </section>
    )
  }
  const [firstCompany] = companies
  const companyId = +searchParams.companyId || firstCompany.id
  const currentCompany = companies.find((c) => c.id === companyId)
  const response = await getLeads({ companyId, searchParams })

  return (
    <section className="section-py min-h-[50vh]">
      <div className="container flex flex-col">
        <h2 className="section-title text-center">Unassigned leads</h2>
        {!!currentCompany && !!response && (
          <>
            <div className="flex items-center justify-between gap-8 flex-wrap mb-6">
              <div className="flex items-center gap-8 flex-wrap">
                <CompanySwitcher
                  companies={companies}
                  currentCompany={currentCompany}
                />
                <Search defaultValue={searchParams.first_name} />
              </div>
              <LinkButton
                href={ROUTES.NEW_LEAD + `?companyId=${companyId}`}
                color="primary"
              >
                Create new lead
              </LinkButton>
            </div>
            <Table entities={response.data} meta={response.meta} />
          </>
        )}
      </div>
    </section>
  )
}
