import { getCompaniesList, getCompany } from '@/app/api/companies'
import { QueryNames } from '@/hooks/useTable'
import { Input } from '@nextui-org/input'
import { Button } from '@nextui-org/button'
import { Select } from '@/components/ui/Select'
import { Source } from '@/types/api/leads'
import { createLead } from '@/app/api/leads'

type Props = {
  searchParams: Record<Partial<QueryNames>, string>
}

const sources: Source[] = ['Instagram', 'Twitter', 'Youtube', 'Other']
const sourceOptions = sources.map((el) => ({ id: el, name: el }))

export default async function Page({ searchParams }: Props) {
  const companies = (await getCompaniesList()) ?? []

  if (companies.length === 0) {
    return (
      <section className="section-py min-h-[50vh]">
        <div className="container flex flex-col">
          <h2 className="section-title text-center">Create new lead</h2>
          <div>Create first company to start</div>
        </div>
      </section>
    )
  }

  const [firstCompany] = companies
  const companyId = +searchParams.companyId || firstCompany.id
  const currentCompany = await getCompany(companyId)

  return (
    <section className="section-py min-h-[50vh]">
      <div className="container flex flex-col">
        <h2 className="section-title text-center">Create new lead</h2>
        {!!currentCompany && (
          <>
            <p className="text-center mb-10">
              For company: {currentCompany.name}
            </p>
            <form
              action={createLead.bind(null, companyId)}
              className="max-w-[500px] w-full mx-auto flex flex-col gap-4"
            >
              <Input variant="flat" label={'First name'} name="first_name" />
              <Input variant="flat" label={'Last name'} name="last_name" />
              <Select
                name="board_id"
                label="Board"
                placeholder="Choose board"
                options={currentCompany.boards}
              />
              <Select
                name="status_id"
                label="Status"
                placeholder="Choose status"
                options={currentCompany.statuses}
              />
              <Select
                name="source"
                label="Source"
                placeholder="Choose source"
                options={sourceOptions}
              />

              <Button type="submit" className="mt-10 mx-auto block">
                Create
              </Button>
            </form>
          </>
        )}
      </div>
    </section>
  )
}
