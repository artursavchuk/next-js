import { LinkButton } from '@/components/ui/LinkButton'
import { ROUTES } from '@/libs/router'
import { useTranslations } from 'next-intl'

export default function Page() {
  const t = useTranslations()
  return (
    <section className="min-h-[50dvh] section-py text-center">
      <div className="container">
        <h1 className="section-title">{t('section_404.title')}</h1>
        <p className="mb-10">{t('section_404.desc')}</p>
        <LinkButton href={ROUTES.HOME} color="primary">
          home
        </LinkButton>
      </div>
    </section>
  )
}
