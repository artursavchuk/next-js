import { getProfile } from '@/app/api/profile'
import { ProfileForm, Translations } from '@/components/ui/ProfileForm'
import { useValidation } from '@/hooks/useValidation'
import { Metadata } from 'next'
import { getTranslations } from 'next-intl/server'

export const generateMetadata = async (): Promise<Metadata> => {
  const t = await getTranslations()
  return {
    title: t('metadata.profile.title'),
  }
}

export default async function Page() {
  const t = await getTranslations()
  const profile = await getProfile()
  const { rules } = useValidation()

  if (!profile) return

  const translations: Translations = {
    labels: {
      name: t('form.name'),
      email: t('form.email'),
      password: t('form.password'),
      confirmation: t('form.confirmation'),
    },
    validations: {
      required: t('errors.required'),
      email: t('errors.email'),
      nameMinLength: `${t('errors.min')} ${rules.nameMinLength}`,
      passwordMinLength: `${t('errors.min')} ${rules.passwordMinLength}`,
      passwordMaxLength: `${t('errors.max')} ${rules.passwordMaxLength}`,
      samePasswords: t('errors.samePasswords'),
    },
    submit: t('buttons.save'),
  }

  return (
    <section className="section-py min-h-[50vh]">
      <div className="container">
        <div className="max-w-[500px] mx-auto">
          <h2 className="section-title text-center">
            {t('section_profile.title')}
          </h2>
          <ProfileForm t={translations} profile={profile} />
        </div>
      </div>
    </section>
  )
}
