import { getCompany } from '@/app/api/companies'
import { CompanyForm, Translations } from '@/components/ui/CompanyForm'
import { Metadata } from 'next'
import { getTranslations } from 'next-intl/server'

type Props = {
  params: {
    id: string
  }
}

export const generateMetadata = async ({
  params,
}: Props): Promise<Metadata> => {
  const t = await getTranslations()
  const company = await getCompany(+params.id)
  if (!company) return {}

  return {
    title: t('metadata.edit_company.title', { name: company.name }),
  }
}

export default async function Page({ params }: Props) {
  const t = await getTranslations()
  const company = await getCompany(+params.id)

  if (company === null) return

  const translations: Translations = {
    labels: {
      companyName: t('form.companyName'),
      boardName: t('form.boardName'),
    },
    validations: {
      required: t('errors.required'),
    },
    submit: t('buttons.update'),
  }

  return (
    <section className="section-py min-h-[50vh]">
      <div className="container">
        <h2 className="section-title text-center">
          {t('section_edit_company.title', { name: company.name })}
        </h2>
        <div className="max-w-[500px] mx-auto">
          <CompanyForm t={translations} company={company} />
        </div>
      </div>
    </section>
  )
}
