import { CompanyForm, Translations } from '@/components/ui/CompanyForm'
import { Metadata } from 'next'
import { useTranslations } from 'next-intl'
import { getTranslations } from 'next-intl/server'

export const generateMetadata = async (): Promise<Metadata> => {
  const t = await getTranslations()
  return {
    title: t('metadata.new_company.title'),
  }
}

export default function Page() {
  const t = useTranslations()

  const translations: Translations = {
    labels: {
      companyName: t('form.companyName'),
      boardName: t('form.boardName'),
    },
    validations: {
      required: t('errors.required'),
    },
    submit: t('buttons.create'),
  }

  return (
    <section className="section-py min-h-[50vh]">
      <div className="container">
        <h2 className="section-title text-center">
          {t('section_new_company.title')}
        </h2>
        <div className="max-w-[500px] mx-auto">
          <CompanyForm t={translations} />
        </div>
      </div>
    </section>
  )
}
