import { getCompaniesList, getCompany } from '@/app/api/companies'
import { ROUTES } from '@/libs/router'
import { getTranslations } from 'next-intl/server'
import {
  CompanyCard,
  Translations as CardTranslations,
} from '@/components/ui/CompanyCard'
import {
  ImportButton,
  Translations as ImportTranslations,
} from '@/components/ui/ImportButton'
import { Company } from '@/types/api/companies'
import { LinkButton } from '@/components/ui/LinkButton'
import { Metadata } from 'next'

export const generateMetadata = async (): Promise<Metadata> => {
  const t = await getTranslations()
  return {
    title: t('metadata.companies.title'),
  }
}

export default async function Page() {
  const t = await getTranslations()
  const companies = await getCompaniesList()
  let companyForImport: null | Company = null

  if (companies === null) return

  if (companies.length) {
    companyForImport = await getCompany(companies[0].id)
  }

  const importTranslations: ImportTranslations = {
    import: t('buttons.import'),
    uploadFile: t('buttons.uploadFile'),
    beginImport: t('buttons.beginImport'),
    toast: { sent: t('toasts.sent') },
  }

  const cardTranslations: CardTranslations = {
    edit: t('buttons.edit'),
    delete: t('buttons.delete'),
  }

  return (
    <section className="section-py min-h-[50vh]">
      <div className="container flex flex-col">
        <h2 className="section-title text-center">
          {t('section_companies.title', { count: companies.length })}
        </h2>

        <div className="flex items-center gap-8 mb-10 justify-center">
          <LinkButton href={ROUTES.NEW_COMPANY} color="primary">
            {t('buttons.create')}
          </LinkButton>
          {companyForImport && (
            <ImportButton t={importTranslations} company={companyForImport} />
          )}
        </div>

        <div className="grid items-center gap-8 grid-cols-[repeat(auto-fill,_minmax(300px,_1fr))]">
          {companies.map((company) => {
            return (
              <CompanyCard
                key={company.id}
                company={company}
                t={cardTranslations}
              />
            )
          })}
        </div>
      </div>
    </section>
  )
}
