'use server'

import { cookies } from 'next/headers'

export const getToken = () => {
  const cookiesStore = cookies()
  const token = cookiesStore.get('t')?.value
  return token
}
