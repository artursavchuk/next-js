import { QueryParameter } from '@/hooks/useTable'
import { Lead } from '@/types/api/leads'
import { SortDescriptor } from '@nextui-org/react'
import { SortDirection } from '@react-types/shared'

export const getColumnValue = (el: Lead, key: string | number | bigint) => {
  let value = null
  if (typeof key === 'string' && key in el) {
    value = el[key as keyof Lead]
  }

  return value ?? '---'
}
