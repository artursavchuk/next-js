import { FormValues } from '@/components/ui/CompanyForm'
import { ErrorResponse } from '@/types/api'
import { CompanyRequest } from '@/types/api/companies'

export const getResponseMessage = (response: ErrorResponse) => {
  const messages = Object.values(response.messages ?? {}).flat()
  return messages.length > 0 ? messages[0] : response.title
}

export const generateCompanyRequest = (fv: FormValues): CompanyRequest => {
  return {
    name: fv.companyName,
    boards: [
      {
        name: fv.boardName,
        currencies: [{ name: 'USD' }],
        payment_types: [{ name: 'cash' }],
      },
    ],
  }
}

export const generateCsvFormData = (file: File, statusId: number) => {
  const fd = new FormData()
  fd.append('file', file)
  fd.append('status_id', `${statusId}`)
  fd.append('priority_id', '3')
  fd.append('replacements[0][excel_column_name]', 'first_name')
  fd.append('replacements[0][raw_column_name]', 'first_name')
  fd.append('replacements[1][excel_column_name]', 'last_name')
  fd.append('replacements[1][raw_column_name]', 'last_name')
  return fd
}
