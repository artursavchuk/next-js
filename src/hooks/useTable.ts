'use client'

import { usePathname, useRouter } from '@/navigation'
import { useSearchParams } from 'next/navigation'
import { useCallback } from 'react'

export type QueryNames =
  | 'page'
  | 'first_name'
  | 'companyId'
  | 'sort'
  | 'direction'

export type QueryParameter = {
  name: QueryNames
  value: string | number
}
type Args = QueryParameter | QueryParameter[]

export const useTable = () => {
  const router = useRouter()
  const pathname = usePathname()
  const searchParams = useSearchParams()

  const createQueryString = useCallback(
    (args: Args) => {
      const params = new URLSearchParams(searchParams.toString())

      const setParameter = (name: QueryNames, value: string | number) => {
        if (value === '') {
          params.delete(name)
        } else {
          params.set(name, `${value}`)
        }
      }

      if (Array.isArray(args)) {
        args.forEach(({ name, value }) => setParameter(name, value))
      } else {
        setParameter(args.name, args.value)
      }

      return params.toString()
    },
    [searchParams]
  )

  const updateRoute = (args: Args) => {
    router.push(pathname + '?' + createQueryString(args))
  }

  return {
    updateRoute,
  }
}
