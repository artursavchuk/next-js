export const useValidation = () => {
  const rules = {
    nameMinLength: 3,
    email: /^[A-Za-z0-9._%+-]{1,64}@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/i,
    passwordMinLength: 8,
    passwordMaxLength: 32,
  }

  return {
    rules,
  }
}
