import createIntlMiddleware from 'next-intl/middleware'
import { NextRequest, NextResponse } from 'next/server'
import { locales } from '@/i18n'
import { ROUTES, isAuthRoute, isPrivateRoute } from './libs/router'
import { getToken } from './helpers/auth'

export default async function middleware(request: NextRequest) {
  const token = getToken()

  if (isAuthRoute(request) && token) {
    return NextResponse.redirect(new URL(ROUTES.HOME, request.url))
  }

  if (isPrivateRoute(request) && !token) {
    return NextResponse.redirect(new URL(ROUTES.LOGIN, request.url))
  }

  const handleI18nRouting = createIntlMiddleware({
    locales,
    defaultLocale: 'en',
    localePrefix: 'as-needed',
  })

  const response = handleI18nRouting(request)
  return response
}

export const config = {
  matcher: ['/', '/(en|ru)/:path*', '/((?!_next|_vercel|.*\\..*).*)'],
}
