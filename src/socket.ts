import Echo from 'laravel-echo'
import Pusher from 'pusher-js'
import { getToken } from './helpers/auth'

export let echo: Echo | null = null

export const setupWebSocket = async () => {
  const token = await getToken() // await has effect

  if (echo === null && token) {
    const pusher = new Pusher(process.env.NEXT_PUBLIC_SOCKET_KEY, {
      cluster: process.env.NEXT_PUBLIC_SOCKET_CLUSTER,
      wsHost: process.env.NEXT_PUBLIC_SOCKET_HOST,
      wssPort: process.env.NEXT_PUBLIC_SOCKET_PORT,
      authEndpoint: process.env.NEXT_PUBLIC_AUTH_URL + 'broadcasting/auth',
      forceTLS: true,
      disableStats: true,
      auth: {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    })

    echo = new Echo({
      broadcaster: 'pusher',
      client: pusher,
    })
  }
}
