import { locales } from '@/i18n'
import { NextRequest } from 'next/server'

export const ROUTES = {
  HOME: '/',
  LOGIN: '/login',
  REGISTRATION: '/registration',
  PROFILE: '/profile',
  CONTACTS: '/contacts',
  COMPANIES: '/companies',
  NEW_COMPANY: '/companies/new',
  EDIT_COMPANY: (id: string | number) => `/companies/${id}`,
  LEADS: '/leads',
  NEW_LEAD: '/leads/create-new',
}

export const AUTH_ROUTES = [ROUTES.LOGIN, ROUTES.REGISTRATION]

export const PRIVATE_ROUTES = [ROUTES.PROFILE, ROUTES.COMPANIES]

const getPathnameRegex = (routesList: string[]) => {
  return RegExp(
    `^(/(${locales.join('|')}))?(${routesList
      .flatMap((r) => (r === '/' ? ['', '/'] : r))
      .join('|')})/?$`,
    'i'
  )
}

export const isAuthRoute = (request: NextRequest) => {
  const regex = getPathnameRegex(AUTH_ROUTES)
  return regex.test(request.nextUrl.pathname)
}

export const isPrivateRoute = (request: NextRequest) => {
  const regex = getPathnameRegex(PRIVATE_ROUTES)
  return regex.test(request.nextUrl.pathname)
}
