import { ReactNode } from 'react'
import { Locales } from './i18n'

export interface ILayoutProps {
  children: ReactNode
  params: {
    locale: Locales
  }
}
