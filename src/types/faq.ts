export type FaqItem = {
  title: string
  desc: string
}
