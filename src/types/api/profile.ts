export type Profile = {
  id: number
  name: string
  email: string
  roles: ('owner' | 'manager' | 'employee')[]
  photo: null | string
  is_on_hold: boolean
  can_edit: boolean
  locale: string
  settings: Record<string, unknown>
}
