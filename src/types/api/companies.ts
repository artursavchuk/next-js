import { Profile } from './profile'

export type Currency = {
  name: string
}

export type PaymentType = {
  name: string
}

export type Board = {
  id: number
  name: string
  currencies: Currency[]
  payment_types: PaymentType[]
}

export type Status = {
  id: number
  name: string
}

export type Company = {
  id: number
  name: string
  owner: {
    id: number
    name: string
    roles: ['owner']
  }
  boards: Board[]
  employees: Profile[]
  statuses: Status[]
}

export type CompanyRequest = {
  name: string
  boards: Omit<Board, 'id'>[]
}

export type UpdateCompanyRequest = {
  id: number
  fv: CompanyRequest
}
