export type ImportRequest = {
  boardId: number
  fd: FormData
}

export type ImportResponse = {
  file_id: number
}
