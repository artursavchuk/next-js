import { Board, Status } from './companies'
import { ResponseDataWithMeta } from './index'

export type LeadsRequest<T extends object> = {
  companyId: number | string
  searchParams?: T
}

export type Lead = {
  id: number
  first_name: string
  last_name: string
  source: string
  email?: string
  phone?: string
}

export type LeadsResponse = ResponseDataWithMeta<Lead[]>

export type Source = 'Twitter' | 'Instagram' | 'Youtube' | 'Other'

export type CreateLeadRequest = {
  boardId: Board['id']
  fv: {
    first_name: string
    last_name: string
    source: Source
    status_id: Status['id']
  }
}
