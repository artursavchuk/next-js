export type Meta = {
  total: number
  per_page: number
  from: number
  to: number
  current_page: number
  last_page: number
}

export type ResponseData<T> = {
  data: T
}

export type ResponseDataWithMeta<T> = {
  data: T
  meta: Meta
}

export type SuccessResponse<T> = {
  success: true
  data: T
}

export type ErrorResponse = {
  success: false
  title: string
  messages: Record<string, string[]>
}

export type BaseResponse<T> = SuccessResponse<T> | ErrorResponse
