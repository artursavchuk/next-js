import { locales } from '@/i18n'

export type Locales = (typeof locales)[number]
