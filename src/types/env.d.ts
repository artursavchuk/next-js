export declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NEXT_PUBLIC_API_URL: string
      NEXT_PUBLIC_SOCKET_CLUSTER: string
      NEXT_PUBLIC_SOCKET_PORT: number
      NEXT_PUBLIC_SOCKET_KEY: string
      NEXT_PUBLIC_SOCKET_HOST: string
    }
  }
}
