import { Button } from '@nextui-org/button'
import { useTranslations } from 'next-intl'
import { ModalTriggerButton } from '../ui/ModalTriggerButton'
import { LoginForm, Translations } from '../ui/LoginForm'
import { useValidation } from '@/hooks/useValidation'
import { getToken } from '@/helpers/auth'

export const Hero = () => {
  const t = useTranslations()
  const token = getToken()
  const { rules } = useValidation()

  const translations: Translations = {
    labels: {
      email: t('form.email'),
      password: t('form.password'),
    },
    validations: {
      required: t('errors.required'),
      email: t('errors.email'),
      passwordMinLength: `${t('errors.min')} ${rules.passwordMinLength}`,
      passwordMaxLength: `${t('errors.max')} ${rules.passwordMaxLength}`,
    },
    submit: t('buttons.sign_in'),
  }

  return (
    <section className="min-h-[100dvh] pt-[20dvh] hero">
      <div className="container relative">
        <div className="sm:max-w-[75%]">
          <h1 className="text-5xl md:text-7xl mb-8 xl:mb-16">
            {t('section_hero_home.title')}
          </h1>
          <p className="mb-8">{t('section_hero_home.desc')}</p>

          {!token && (
            <ModalTriggerButton buttonValue={t('buttons.start')}>
              <LoginForm t={translations} />
            </ModalTriggerButton>
          )}
        </div>
      </div>
    </section>
  )
}
