import { useTranslations } from 'next-intl'

export const Services = () => {
  const t = useTranslations()
  return (
    <section className="section-py min-h-[50vh]">
      <div className="container">
        <div className="max-w-[800px] mx-auto text-center">
          <h2 className="section-title">{t('section_services.title')}</h2>
          <p className="mb-4">{t('section_services.desc_1')}</p>
          <p>{t('section_services.desc_2')}</p>
        </div>
      </div>
    </section>
  )
}
