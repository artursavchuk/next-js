import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  // Image,
  Button,
} from '@nextui-org/react'

import { useTranslations } from 'next-intl'
import Image from 'next/image'

export const Research = () => {
  const t = useTranslations()
  return (
    <section className="section-py text-center">
      <div className="container">
        <h2 className="section-title">{t('section_research.title')}</h2>
        <p className="max-w-[800px] mx-auto mb-8">
          {t('section_research.desc')}
        </p>

        <div className="grid grid-cols-1 sm:grid-cols-2 gap-4 max-w-[800px] mx-auto">
          <Card className="min-h-[250px]">
            <CardHeader className="absolute z-10 top-1 flex-col">
              <h3 className="mb-6 text-white font-medium text-2xl">
                {t('section_research.card_1.title')}
              </h3>
              <p className="text-white font-medium">
                {t('section_research.card_1.desc')}
              </p>
            </CardHeader>
            <Image
              src="/research_1.svg"
              alt="card background"
              fill
              className="object-cover"
            />
          </Card>
          <Card className="min-h-[250px]">
            <CardHeader className="absolute z-10 top-1 flex-col">
              <h3 className="mb-6 text-white font-medium text-2xl">
                {t('section_research.card_2.title')}
              </h3>
              <p className="text-white font-medium">
                {t('section_research.card_2.desc')}
              </p>
            </CardHeader>
            <Image
              src="/research_2.svg"
              alt="card background"
              fill
              className="object-cover"
            />
          </Card>
          <Card className="min-h-[250px]">
            <CardHeader className="absolute z-10 top-1 flex-col">
              <h3 className="mb-6 text-white font-medium text-2xl">
                {t('section_research.card_3.title')}
              </h3>
              <p className="text-white font-medium">
                {t('section_research.card_3.desc')}
              </p>
            </CardHeader>
            <Image
              src="/research_3.svg"
              alt="card background"
              fill
              className="object-cover"
            />
          </Card>
          <Card className="min-h-[250px]">
            <CardHeader className="absolute z-10 top-1 flex-col">
              <h3 className="mb-6 text-white font-medium text-2xl">
                {t('section_research.card_4.title')}
              </h3>
              <p className="text-white font-medium">
                {t('section_research.card_4.desc')}
              </p>
            </CardHeader>
            <Image
              src="/research_4.svg"
              alt="card background"
              fill
              className="object-cover"
            />
          </Card>
        </div>
      </div>
    </section>
  )
}
