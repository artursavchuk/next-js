import { useTranslations } from 'next-intl'
import { FaqAccordion } from '../ui/FaqAccordion'
import { FaqItem } from '@/types/faq'

export const Faq = () => {
  const t = useTranslations()
  const data: FaqItem[] = [
    {
      title: t('section_faq.card_1.title'),
      desc: t('section_faq.card_1.desc'),
    },
    {
      title: t('section_faq.card_2.title'),
      desc: t('section_faq.card_2.desc'),
    },
    {
      title: t('section_faq.card_3.title'),
      desc: t('section_faq.card_3.desc'),
    },
    {
      title: t('section_faq.card_4.title'),
      desc: t('section_faq.card_4.desc'),
    },
  ]

  return (
    <section className="section-py min-h-[50vh]">
      <div className="container">
        <div className="max-w-[800px] mx-auto">
          <h2 className="section-title text-center">
            {t('section_faq.title')}
          </h2>

          <FaqAccordion faq={data} />
        </div>
      </div>
    </section>
  )
}
