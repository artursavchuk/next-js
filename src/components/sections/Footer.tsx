import { Link } from '@/navigation'
import { useTranslations } from 'next-intl'
import Image from 'next/image'

export const Footer = () => {
  const t = useTranslations()
  return (
    <footer className="py-5 footer bg-primary-400 text-white">
      <div className="container">
        <div className="flex gap-4 flex-col items-center sm:flex-row sm:justify-between mb-4">
          <Link href="/">
            <Image
              src="/logo-white.svg"
              alt="logo"
              width={120}
              height={30}
              className="w-[120px] h-[30px]"
            />
          </Link>

          <nav>
            <ul className="flex items-center gap-4 flex-col min-[400px]:flex-row">
              <li>
                <Link className="with-hover" href="/">
                  {t('menu.policy')}
                </Link>
              </li>
              <li>
                <Link className="with-hover" href="/">
                  {t('menu.terms')}
                </Link>
              </li>
            </ul>
          </nav>
        </div>
        <p className="text-center">{t('footer.copyright')}</p>
      </div>
    </footer>
  )
}
