import Image from 'next/image'
import {
  Navbar,
  NavbarBrand,
  NavbarContent,
  NavbarMenuToggle,
  NavbarItem,
} from '@nextui-org/react'
import { MobileMenu, Translations as MenuTranslations } from '../ui/MobileMenu'
import { ROUTES } from '@/libs/router'
import {
  HeaderProfile,
  Translations as ProfileTranslations,
} from '../ui/HeaderProfile'
import { getLocale, getTranslations } from 'next-intl/server'
import { getProfile } from '@/app/api/profile'
import { Link } from '@/navigation'
import { LinkButton } from '../ui/LinkButton'
import { LanguageSwitcher } from '../ui/LanguageSwitcher'
import { Locales } from '@/types/i18n'

export const Header = async () => {
  const locale = (await getLocale()) as Locales
  const profile = await getProfile()
  const t = await getTranslations()

  const profileTranslations: ProfileTranslations = {
    profile: t('menu.profile'),
    companies: t('menu.companies'),
    leads: t('menu.leads'),
    sign_out: t('buttons.sign_out'),
  }
  const menuTranslations: MenuTranslations = {
    sign_in: t('buttons.sign_in'),
    sign_up: t('buttons.sign_up'),
    sign_out: t('buttons.sign_out'),
    companies: t('menu.companies'),
    profile: t('menu.profile'),
    leads: t('menu.leads'),
  }

  return (
    <Navbar
      maxWidth="full"
      className="bg-transparent"
      classNames={{ wrapper: 'px-0' }}
    >
      <div className="container flex gap-4 items-center justify-between">
        <NavbarContent>
          <NavbarBrand>
            <Link href={ROUTES.HOME}>
              <Image
                src="/logo.svg"
                alt="logo"
                width={120}
                height={30}
                className="w-[120px] h-[30px]"
              />
            </Link>
          </NavbarBrand>
        </NavbarContent>

        <NavbarContent justify="end" className="hidden sm:flex">
          <LanguageSwitcher locale={locale} />
          {!!profile && (
            <HeaderProfile
              profile={profile}
              locale={locale}
              t={profileTranslations}
            />
          )}

          {!profile && (
            <>
              <NavbarItem>
                <Link className="with-hover" href={ROUTES.LOGIN}>
                  {t('buttons.sign_in')}
                </Link>
              </NavbarItem>

              <NavbarItem>
                <LinkButton
                  color="primary"
                  href={ROUTES.REGISTRATION}
                  variant="flat"
                >
                  {t('buttons.sign_up')}
                </LinkButton>
              </NavbarItem>
            </>
          )}
        </NavbarContent>

        <div className="flex items-center gap-4 sm:hidden">
          <LanguageSwitcher locale={locale} />
          <NavbarMenuToggle className="h-6 text-primary-500" />
        </div>

        <MobileMenu profile={profile} t={menuTranslations} />
      </div>
    </Navbar>
  )
}
