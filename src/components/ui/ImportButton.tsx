'use client'

import { importLeads } from '@/app/api/import'
import { generateCsvFormData, getResponseMessage } from '@/helpers/api'
import { echo } from '@/socket'
import { Company } from '@/types/api/companies'
import { ChangeEvent, useRef, useState } from 'react'
import { toast } from 'sonner'
import {
  Button,
  Modal,
  ModalContent,
  ModalBody,
  useDisclosure,
} from '@nextui-org/react'
import clsx from 'clsx'

export type Translations = {
  import: string
  uploadFile: string
  beginImport: string
  toast: {
    sent: string
  }
}

type Props = {
  t: Translations
  company: Company
}

export const ImportButton = ({ t, company }: Props) => {
  const { isOpen, onOpen, onOpenChange } = useDisclosure()
  const [file, setFile] = useState<File | null>(null)
  const inputRef = useRef<HTMLInputElement>(null)

  const handleFileChange = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files) {
      setFile(e.target.files[0])
    }
  }

  const subscribeWS = (id: number) => {
    if (!echo) return
    const channelName = `imports.${id}`
    const eventName = '.FileImportFinishedEvent'
    const channel = echo.private(channelName)
    channel.listen(eventName, () => {
      toast.success('Finished import')
      channel.stopListening(eventName)
    })
  }

  const handleClick = async () => {
    if (!file) return
    const boardId = company.boards[0].id
    const statusId = company.statuses[0].id
    const fd = generateCsvFormData(file, statusId)
    const response = await importLeads({ boardId, fd })

    if (response === null) return
    if (!response.success) {
      toast.error(getResponseMessage(response))
      return
    }

    toast.success(t.toast.sent)
    subscribeWS(response.data.file_id)
  }

  return (
    <>
      <Button variant="ghost" color="primary" onPress={onOpen}>
        {t.import}
      </Button>
      <Modal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        onClose={() => setFile(null)}
      >
        <ModalContent>
          {(onClose) => (
            <>
              <ModalBody className="py-14 px-8">
                <input
                  ref={inputRef}
                  type="file"
                  name="file"
                  hidden
                  onChange={handleFileChange}
                />
                {!file && (
                  <Button
                    color="primary"
                    onPress={() => inputRef.current?.click()}
                  >
                    {t.uploadFile}
                  </Button>
                )}
                {!!file && (
                  <h4 className="section-title text-center">{file.name}</h4>
                )}
                <Button
                  variant={file ? 'solid' : 'ghost'}
                  color={file ? 'primary' : 'default'}
                  className={clsx({ 'cursor-not-allowed': !file })}
                  disabled={!file}
                  onPress={handleClick}
                >
                  {t.beginImport}
                </Button>
              </ModalBody>
            </>
          )}
        </ModalContent>
      </Modal>
    </>
  )
}
