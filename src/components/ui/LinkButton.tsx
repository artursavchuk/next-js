'use client'

import { useRouter } from '@/navigation'
import { Button } from '@nextui-org/button'
import { ComponentProps } from 'react'

export const LinkButton = ({
  href,
  ...rest
}: ComponentProps<typeof Button>) => {
  const router = useRouter()

  const handleClick = () => {
    if (!href) return
    router.push(href)
  }

  return <Button onClick={handleClick} {...rest} />
}
