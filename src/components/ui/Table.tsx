'use client'

import { getColumnValue } from '@/helpers/table'
import { useTable } from '@/hooks/useTable'
import { Meta } from '@/types/api'
import { Lead } from '@/types/api/leads'
import {
  Table as TableComponent,
  TableHeader,
  TableBody,
  TableColumn,
  TableRow,
  TableCell,
  Pagination,
} from '@nextui-org/react'
import { SortButton } from './SortButton'

type Props = {
  entities: Lead[]
  meta: Meta
}

export const Table = (props: Props) => {
  const { entities, meta } = props
  const { updateRoute } = useTable()

  return (
    <TableComponent
      aria-label="table"
      isHeaderSticky
      bottomContent={
        <div className="flex w-full justify-center">
          <Pagination
            isCompact
            showControls
            showShadow
            color="secondary"
            page={meta.current_page}
            total={meta.last_page}
            onChange={(page) => updateRoute({ name: 'page', value: page })}
          />
        </div>
      }
      bottomContentPlacement="outside"
      classNames={{ wrapper: 'max-h-[50vh] overflow-auto' }}
    >
      <TableHeader>
        <TableColumn key="first_name">
          <div className="flex items-center gap-2">
            <span>first_name</span>
            <SortButton name="first_name" />
          </div>
        </TableColumn>
        <TableColumn key="last_name">
          <div className="flex items-center gap-2">
            <span>last_name</span>
            <SortButton name="last_name" />
          </div>
        </TableColumn>
        <TableColumn key="email">email</TableColumn>
        <TableColumn key="phone">phone</TableColumn>
        <TableColumn key="source">source</TableColumn>
      </TableHeader>

      <TableBody items={entities}>
        {(el) => (
          <TableRow key={el.id}>
            {(columnKey) => (
              <TableCell>{getColumnValue(el, columnKey)}</TableCell>
            )}
          </TableRow>
        )}
      </TableBody>
    </TableComponent>
  )
}
