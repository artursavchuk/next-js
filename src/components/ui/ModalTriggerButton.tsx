'use client'
import { PropsWithChildren } from 'react'
import {
  Modal,
  ModalContent,
  ModalBody,
  Button,
  useDisclosure,
  ButtonProps,
} from '@nextui-org/react'

type Props = {
  buttonValue: string
}

export const ModalTriggerButton = ({
  children,
  buttonValue,
  ...buttonProps
}: PropsWithChildren<Props & ButtonProps>) => {
  const { isOpen, onOpen, onOpenChange } = useDisclosure()

  return (
    <>
      <Button color="primary" {...buttonProps} onClick={onOpen}>
        {buttonValue}
      </Button>

      <Modal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        backdrop="opaque"
        classNames={{
          backdrop:
            'bg-gradient-to-t from-zinc-900 to-zinc-900/10 backdrop-opacity-20',
        }}
      >
        <ModalContent className="py-10">
          <ModalBody>{children}</ModalBody>
        </ModalContent>
      </Modal>
    </>
  )
}
