'use client'

import EyeOpenedIcon from '/public/eye-opened.svg'
import EyeClosedIcon from '/public/eye-closed.svg'
import clsx from 'clsx'
import { SubmitHandler, useForm } from 'react-hook-form'
import { Button, Input } from '@nextui-org/react'
import { useValidation } from '@/hooks/useValidation'
import { useState } from 'react'
import { ROUTES } from '@/libs/router'
import { toast } from 'sonner'
import { getResponseMessage } from '@/helpers/api'
import { getProfile } from '@/app/api/profile'
import { login } from '@/app/api/auth'
import { useRouter } from '@/navigation'

export type Translations = {
  labels: {
    email: string
    password: string
  }
  validations: {
    required: string
    email: string
    passwordMinLength: string
    passwordMaxLength: string
  }
  submit: string
}

type Props = {
  t: Translations
}

export type FormValues = {
  email: string
  password: string
}

export const LoginForm = ({ t }: Props) => {
  const router = useRouter()
  const [isVisible, setIsVisible] = useState(false)
  const { rules } = useValidation()
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormValues>()

  const submit: SubmitHandler<FormValues> = async (fd) => {
    const response = await login(fd)
    if (!response.success) {
      toast.error(getResponseMessage(response))
      return
    }

    const profile = await getProfile()
    if (profile) {
      router.push(ROUTES.HOME)
    }
  }

  return (
    <form onSubmit={handleSubmit(submit)} className="flex flex-col gap-6">
      <Input
        type="email"
        variant="flat"
        labelPlacement="outside"
        label={t.labels.email}
        isInvalid={!!errors.email?.message}
        errorMessage={errors.email?.message}
        color={!!errors.email?.message ? 'danger' : 'default'}
        {...register('email', {
          required: t.validations.required,
          pattern: {
            value: rules.email,
            message: t.validations.email,
          },
        })}
      />
      <Input
        type={isVisible ? 'text' : 'password'}
        variant="flat"
        labelPlacement="outside"
        label={t.labels.password}
        isInvalid={!!errors.password?.message}
        errorMessage={errors.password?.message}
        color={!!errors.password?.message ? 'danger' : 'default'}
        endContent={
          <button
            className="focus:outline-none flex items-center justify-center cursor-pointer"
            type="button"
            onClick={() => setIsVisible(!isVisible)}
          >
            <EyeClosedIcon
              className={clsx('w-5 h-5 text-primary-400 pointer-events-none', {
                flex: !isVisible,
                hidden: isVisible,
              })}
            />
            <EyeOpenedIcon
              className={clsx('w-5 h-5 text-primary-400 pointer-events-none', {
                flex: isVisible,
                hidden: !isVisible,
              })}
            />
          </button>
        }
        {...register('password', {
          required: t.validations.required,
          minLength: {
            value: rules.passwordMinLength,
            message: t.validations.passwordMinLength,
          },
          maxLength: {
            value: rules.passwordMaxLength,
            message: t.validations.passwordMaxLength,
          },
        })}
      />

      <Button
        type="submit"
        variant="flat"
        color="primary"
        className="max-w-[250px] w-full mx-auto"
        isDisabled={!!Object.keys(errors).length}
      >
        {t.submit}
      </Button>
    </form>
  )
}
