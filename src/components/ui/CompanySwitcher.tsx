'use client'

import { useTable } from '@/hooks/useTable'
import { Company } from '@/types/api/companies'
import {
  Dropdown,
  DropdownMenu,
  DropdownTrigger,
  DropdownItem,
  Button,
} from '@nextui-org/react'

type Props = {
  companies: Company[]
  currentCompany: Company
}

export const CompanySwitcher = ({ companies, currentCompany }: Props) => {
  const { updateRoute } = useTable()

  const handleSelect = (selected: Company) => {
    updateRoute([
      { name: 'companyId', value: selected.id },
      { name: 'page', value: 1 },
      { name: 'first_name', value: '' },
    ])
  }

  return (
    <Dropdown>
      <DropdownTrigger>
        <Button variant="solid" size="lg" color="secondary">
          {currentCompany.name}
        </Button>
      </DropdownTrigger>

      <DropdownMenu aria-label="Link Actions">
        {companies.map((el) => (
          <DropdownItem key={el.id} onClick={() => handleSelect(el)}>
            {el.name}
          </DropdownItem>
        ))}
      </DropdownMenu>
    </Dropdown>
  )
}
