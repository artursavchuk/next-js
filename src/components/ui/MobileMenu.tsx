'use client'

import { NavbarMenu, NavbarMenuItem, Button } from '@nextui-org/react'
import { ROUTES } from '@/libs/router'
import { Profile } from '@/types/api/profile'
import { logout } from '@/app/api/auth'
import { toast } from 'sonner'
import { getResponseMessage } from '@/helpers/api'
import { Link, useRouter } from '@/navigation'

export type Translations = {
  profile: string
  companies: string
  leads: string
  sign_in: string
  sign_up: string
  sign_out: string
}

type Props = {
  profile: Profile | null
  t: Translations
}

export const MobileMenu = ({ profile, t }: Props) => {
  const router = useRouter()

  const handleLogout = async () => {
    const response = await logout()
    if (response === null) return
    if (!response.success) {
      toast.error(getResponseMessage(response))
      return
    }
    router.push(ROUTES.HOME)
  }

  const menu = [
    {
      label: t.profile,
      href: ROUTES.PROFILE,
    },
    {
      label: t.companies,
      href: ROUTES.COMPANIES,
    },
    {
      label: t.leads,
      href: ROUTES.LEADS,
    },
  ]

  return (
    <NavbarMenu aria-label="mobile menu">
      {!!profile && (
        <>
          {menu.map((el) => (
            <NavbarMenuItem key={el.label}>
              <Link href={el.href}>{el.label}</Link>
            </NavbarMenuItem>
          ))}

          <NavbarMenuItem>
            <Button variant="bordered" color="danger" onClick={handleLogout}>
              {t.sign_out}
            </Button>
          </NavbarMenuItem>
        </>
      )}
      {!profile && (
        <>
          <NavbarMenuItem>
            <Link href={ROUTES.LOGIN}>{t.sign_in}</Link>
          </NavbarMenuItem>
          <NavbarMenuItem>
            <Link href={ROUTES.REGISTRATION}>{t.sign_up}</Link>
          </NavbarMenuItem>
        </>
      )}
    </NavbarMenu>
  )
}
