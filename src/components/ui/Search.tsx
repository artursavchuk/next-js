'use client'
import { useTable } from '@/hooks/useTable'
import { Input } from '@nextui-org/input'
import { FormEvent, useEffect, useState } from 'react'
import IconClose from '/public/close.svg'
import IconSearch from '/public/search.svg'

type Props = {
  defaultValue?: string
}

export const Search = ({ defaultValue = '' }: Props) => {
  const { updateRoute } = useTable()
  const [value, setValue] = useState(defaultValue)

  useEffect(() => {
    setValue(defaultValue)
  }, [defaultValue])

  const handleSubmit = (e: FormEvent) => {
    e.preventDefault()
    updateRoute([
      { name: 'first_name', value },
      { name: 'page', value: 1 },
    ])
  }

  const handleClear = () => {
    setValue('')
    updateRoute([
      { name: 'first_name', value: '' },
      { name: 'page', value: 1 },
    ])
  }

  return (
    <form onSubmit={handleSubmit}>
      <Input
        variant="bordered"
        label="Search by first_name"
        value={value}
        onChange={(e) => setValue(e.target.value)}
        endContent={
          <>
            {!!value && (
              <div className="flex items-center gap-2">
                <button type="button" onClick={handleClear}>
                  <IconClose className="size-5" />
                </button>
                <button type="submit">
                  <IconSearch className="size-5" />
                </button>
              </div>
            )}
          </>
        }
      />
    </form>
  )
}
