'use client'

import { Button } from '@nextui-org/button'
import { useSearchParams } from 'next/navigation'
import IconArrow from '/public/dropdown-arrow.svg'
import clsx from 'clsx'
import { useTable } from '@/hooks/useTable'

type Props = {
  name: string
}

type Direction = 'asc' | 'desc'

export const SortButton = ({ name }: Props) => {
  const searchParams = useSearchParams()
  const { updateRoute } = useTable()

  const isActive = searchParams.get('sort') === name
  const direction = searchParams.get('direction') as Direction | null

  const handleClick = () => {
    let sortValue = ''
    let sortDirection: Direction | '' = ''

    if (!isActive) {
      sortValue = name
      sortDirection = 'asc'
    }
    if (direction === 'asc') {
      sortValue = name
      sortDirection = 'desc'
    }

    updateRoute([
      { name: 'sort', value: sortValue },
      { name: 'direction', value: sortDirection },
    ])
  }

  return (
    <Button
      size="sm"
      variant={isActive ? 'solid' : 'bordered'}
      color={isActive ? 'secondary' : 'default'}
      className="p-0 size-5 min-w-5 h-5 border-1 rounded-md"
      onClick={handleClick}
    >
      <IconArrow
        className={clsx(
          'size-5',
          { 'rotate-180': !isActive || direction === 'asc' },
          ...[isActive ? 'text-white' : 'text-default-400']
        )}
      />
    </Button>
  )
}
