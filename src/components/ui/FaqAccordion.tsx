'use client'
import { FaqItem } from '@/types/faq'
import { Accordion, AccordionItem, Selection } from '@nextui-org/react'
import { useState } from 'react'

type Props = {
  faq: FaqItem[]
}

export const FaqAccordion = ({ faq }: Props) => {
  const [opened, setOpened] = useState<Selection>(new Set([]))

  const isOpened = (key: string) => {
    return Array.from(opened).some((k) => k === key)
  }

  return (
    <Accordion
      selectionMode="multiple"
      selectedKeys={opened}
      onSelectionChange={setOpened}
    >
      {faq.map((item, i) => {
        return (
          <AccordionItem
            key={String(i + 1)}
            aria-label={item.title}
            classNames={{
              title: isOpened(String(i + 1)) ? 'text-primary-500' : '',
              indicator: isOpened(String(i + 1)) ? 'text-primary-500' : '',
            }}
            title={item.title}
          >
            {item.desc}
          </AccordionItem>
        )
      })}
    </Accordion>
  )
}
