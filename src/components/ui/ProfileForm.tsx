'use client'

import EyeOpenedIcon from '/public/eye-opened.svg'
import EyeClosedIcon from '/public/eye-closed.svg'
import clsx from 'clsx'
import { SubmitHandler, useForm } from 'react-hook-form'
import { Button, Input } from '@nextui-org/react'
import { useValidation } from '@/hooks/useValidation'
import { useEffect, useState } from 'react'
import { toast } from 'sonner'
import { getResponseMessage } from '@/helpers/api'
import { updateProfile } from '@/app/api/profile'
import { Profile } from '@/types/api/profile'

export type Translations = {
  labels: {
    name: string
    email: string
    password: string
    confirmation: string
  }
  validations: {
    required: string
    email: string
    nameMinLength: string
    passwordMinLength: string
    passwordMaxLength: string
    samePasswords: string
  }
  submit: string
}

type Props = {
  t: Translations
  profile: Profile
}

export type FormValues = {
  name: string
  email: string
  password: string
  password_confirmation: string
}

export const ProfileForm = ({ t, profile }: Props) => {
  const [isVisiblePass, setIsVisiblePass] = useState(false)
  const [isVisibleConf, setIsVisibleConf] = useState(false)
  const { rules } = useValidation()
  const {
    register,
    handleSubmit,
    getValues,
    resetField,
    trigger,
    watch,
    formState: { errors },
  } = useForm<FormValues>()

  const submit: SubmitHandler<FormValues> = async (fv) => {
    const fd = new FormData()
    if (fv.name && fv.name !== profile.name) fd.set('name', fv.name)
    if (fv.email && fv.email !== profile.email) fd.set('email', fv.email)
    if (fv.password && fv.password_confirmation) {
      fd.set('password', fv.password)
      fd.set('password_confirmation', fv.password_confirmation)
    }

    const response = await updateProfile(fd)
    if (response === null) return
    if ('success' in response) {
      toast.error(getResponseMessage(response))
    }
  }

  useEffect(() => {
    if (!getValues('password')) {
      resetField('password')
      resetField('password_confirmation')
    }
  }, [watch('password')])

  return (
    <form onSubmit={handleSubmit(submit)} className="flex flex-col gap-6">
      <Input
        variant="flat"
        labelPlacement="outside"
        label={t.labels.name}
        defaultValue={profile.name}
        isInvalid={!!errors.name?.message}
        errorMessage={errors.name?.message}
        color={!!errors.name?.message ? 'danger' : 'default'}
        {...register('name', {
          required: t.validations.required,
          minLength: {
            value: rules.nameMinLength,
            message: t.validations.nameMinLength,
          },
        })}
      />
      <Input
        type="email"
        variant="flat"
        labelPlacement="outside"
        label={t.labels.email}
        defaultValue={profile.email}
        isInvalid={!!errors.email?.message}
        errorMessage={errors.email?.message}
        color={!!errors.email?.message ? 'danger' : 'default'}
        {...register('email', {
          pattern: {
            value: rules.email,
            message: t.validations.email,
          },
        })}
      />
      <Input
        type={isVisiblePass ? 'text' : 'password'}
        variant="flat"
        labelPlacement="outside"
        label={t.labels.password}
        isInvalid={!!errors.password?.message}
        errorMessage={errors.password?.message}
        color={!!errors.password?.message ? 'danger' : 'default'}
        endContent={
          <div
            className="focus:outline-none flex items-center justify-center cursor-pointer"
            onClick={() => setIsVisiblePass(!isVisiblePass)}
          >
            <EyeClosedIcon
              className={clsx('w-5 h-5 text-primary-400 pointer-events-none', {
                flex: !isVisiblePass,
                hidden: isVisiblePass,
              })}
            />
            <EyeOpenedIcon
              className={clsx('w-5 h-5 text-primary-400 pointer-events-none', {
                flex: isVisiblePass,
                hidden: !isVisiblePass,
              })}
            />
          </div>
        }
        {...register('password', {
          minLength: {
            value: rules.passwordMinLength,
            message: t.validations.passwordMinLength,
          },
          maxLength: {
            value: rules.passwordMaxLength,
            message: t.validations.passwordMaxLength,
          },
          validate: (v) => {
            const confirmation = getValues('password_confirmation')
            if (confirmation && v !== confirmation)
              return t.validations.samePasswords
          },
        })}
      />
      {!!watch('password') && (
        <Input
          type={isVisibleConf ? 'text' : 'password'}
          variant="flat"
          labelPlacement="outside"
          label={t.labels.confirmation}
          isInvalid={!!errors.password_confirmation?.message}
          errorMessage={errors.password_confirmation?.message}
          color={!!errors.password_confirmation?.message ? 'danger' : 'default'}
          endContent={
            <div
              className="focus:outline-none flex items-center justify-center cursor-pointer"
              onClick={() => setIsVisibleConf(!isVisibleConf)}
            >
              <EyeClosedIcon
                className={clsx(
                  'w-5 h-5 text-primary-400 pointer-events-none',
                  {
                    flex: !isVisibleConf,
                    hidden: isVisibleConf,
                  }
                )}
              />
              <EyeOpenedIcon
                className={clsx(
                  'w-5 h-5 text-primary-400 pointer-events-none',
                  {
                    flex: isVisibleConf,
                    hidden: !isVisibleConf,
                  }
                )}
              />
            </div>
          }
          {...register('password_confirmation', {
            minLength: {
              value: rules.passwordMinLength,
              message: t.validations.passwordMinLength,
            },
            maxLength: {
              value: rules.passwordMaxLength,
              message: t.validations.passwordMaxLength,
            },
            validate: (v) => {
              const password = getValues('password')
              if (v !== password) return t.validations.samePasswords
              trigger('password')
            },
          })}
        />
      )}

      <Button
        type="submit"
        variant="flat"
        color="primary"
        className="max-w-[250px] w-full mx-auto"
        isDisabled={!!Object.keys(errors).length}
      >
        {t.submit}
      </Button>
    </form>
  )
}
