'use client'

import React from 'react'
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Divider,
  Button,
} from '@nextui-org/react'
import { ROUTES } from '@/libs/router'
import { Company } from '@/types/api/companies'
import { deleteCompany } from '@/app/api/companies'
import { LinkButton } from './LinkButton'

export type Translations = {
  edit: string
  delete: string
}

type Props = {
  company: Company
  t: Translations
}

export const CompanyCard = ({ company, t }: Props) => {
  return (
    <Card className="max-w-[400px]">
      <CardHeader className="flex gap-3">
        <div className="flex flex-col">
          <p className="text-md">{company.name}</p>
        </div>
      </CardHeader>
      <Divider />
      <CardBody>
        {company.boards.map((b) => (
          <p className="text-small text-default-500" key={b.id}>
            {b.name}
          </p>
        ))}
      </CardBody>
      <Divider />
      <CardFooter className="flex items-center gap-8">
        <LinkButton
          href={ROUTES.EDIT_COMPANY(company.id)}
          variant="ghost"
          color="primary"
          className="grow"
        >
          {t.edit}
        </LinkButton>
        <Button
          className="grow"
          color="danger"
          onClick={() => deleteCompany(company.id)}
        >
          {t.delete}
        </Button>
      </CardFooter>
    </Card>
  )
}
