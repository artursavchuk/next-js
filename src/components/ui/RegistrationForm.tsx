'use client'

import EyeOpenedIcon from '/public/eye-opened.svg'
import EyeClosedIcon from '/public/eye-closed.svg'
import clsx from 'clsx'
import { SubmitHandler, useForm } from 'react-hook-form'
import { Button, Input } from '@nextui-org/react'
import { useValidation } from '@/hooks/useValidation'
import { useState } from 'react'
import { ROUTES } from '@/libs/router'
import { getResponseMessage } from '@/helpers/api'
import { toast } from 'sonner'
import { getProfile } from '@/app/api/profile'
import { login, registration } from '@/app/api/auth'
import { useRouter } from '@/navigation'

export type Translations = {
  labels: {
    name: string
    email: string
    password: string
    confirmation: string
  }
  validations: {
    required: string
    email: string
    nameMinLength: string
    passwordMinLength: string
    passwordMaxLength: string
    samePasswords: string
  }
  submit: string
}

type Props = {
  t: Translations
}

export type FormValues = {
  name: string
  email: string
  password: string
  password_confirmation: string
}

export const RegistrationForm = ({ t }: Props) => {
  const router = useRouter()
  const [isVisiblePass, setIsVisiblePass] = useState(false)
  const [isVisibleConf, setIsVisibleConf] = useState(false)
  const { rules } = useValidation()
  const {
    register,
    handleSubmit,
    getValues,
    trigger,
    formState: { errors },
  } = useForm<FormValues>()

  const submit: SubmitHandler<FormValues> = async (fd) => {
    const registrationResponse = await registration(fd)
    if (!registrationResponse.success) {
      toast.error(getResponseMessage(registrationResponse))
      return
    }

    const { email, password } = fd
    const loginResponse = await login({ email, password })
    if (!loginResponse.success) {
      toast.error(getResponseMessage(loginResponse))
      return
    }

    const profile = await getProfile()
    if (profile) {
      router.push(ROUTES.HOME)
    }
  }

  return (
    <form onSubmit={handleSubmit(submit)} className="flex flex-col gap-6">
      <Input
        variant="flat"
        labelPlacement="outside"
        label={t.labels.name}
        isInvalid={!!errors.name?.message}
        errorMessage={errors.name?.message}
        color={!!errors.name?.message ? 'danger' : 'default'}
        {...register('name', {
          required: t.validations.required,
          minLength: {
            value: rules.nameMinLength,
            message: t.validations.nameMinLength,
          },
        })}
      />
      <Input
        type="email"
        variant="flat"
        labelPlacement="outside"
        label={t.labels.email}
        isInvalid={!!errors.email?.message}
        errorMessage={errors.email?.message}
        color={!!errors.email?.message ? 'danger' : 'default'}
        {...register('email', {
          required: t.validations.required,
          pattern: {
            value: rules.email,
            message: t.validations.email,
          },
        })}
      />
      <Input
        type={isVisiblePass ? 'text' : 'password'}
        variant="flat"
        labelPlacement="outside"
        label={t.labels.password}
        isInvalid={!!errors.password?.message}
        errorMessage={errors.password?.message}
        color={!!errors.password?.message ? 'danger' : 'default'}
        endContent={
          <div
            className="focus:outline-none flex items-center justify-center cursor-pointer"
            onClick={() => setIsVisiblePass(!isVisiblePass)}
          >
            <EyeClosedIcon
              className={clsx('w-5 h-5 text-primary-400 pointer-events-none', {
                flex: !isVisiblePass,
                hidden: isVisiblePass,
              })}
            />
            <EyeOpenedIcon
              className={clsx('w-5 h-5 text-primary-400 pointer-events-none', {
                flex: isVisiblePass,
                hidden: !isVisiblePass,
              })}
            />
          </div>
        }
        {...register('password', {
          required: t.validations.required,
          minLength: {
            value: rules.passwordMinLength,
            message: t.validations.passwordMinLength,
          },
          maxLength: {
            value: rules.passwordMaxLength,
            message: t.validations.passwordMaxLength,
          },
          validate: (v) => {
            const confirmation = getValues('password_confirmation')
            if (v !== confirmation) return t.validations.samePasswords
          },
        })}
      />
      <Input
        type={isVisibleConf ? 'text' : 'password'}
        variant="flat"
        labelPlacement="outside"
        label={t.labels.confirmation}
        isInvalid={!!errors.password_confirmation?.message}
        errorMessage={errors.password_confirmation?.message}
        color={!!errors.password_confirmation?.message ? 'danger' : 'default'}
        endContent={
          <div
            className="focus:outline-none flex items-center justify-center cursor-pointer"
            onClick={() => setIsVisibleConf(!isVisibleConf)}
          >
            <EyeClosedIcon
              className={clsx('w-5 h-5 text-primary-400 pointer-events-none', {
                flex: !isVisibleConf,
                hidden: isVisibleConf,
              })}
            />
            <EyeOpenedIcon
              className={clsx('w-5 h-5 text-primary-400 pointer-events-none', {
                flex: isVisibleConf,
                hidden: !isVisibleConf,
              })}
            />
          </div>
        }
        {...register('password_confirmation', {
          required: t.validations.required,
          minLength: {
            value: rules.passwordMinLength,
            message: t.validations.passwordMinLength,
          },
          maxLength: {
            value: rules.passwordMaxLength,
            message: t.validations.passwordMaxLength,
          },
          validate: (v) => {
            const password = getValues('password')
            if (v !== password) return t.validations.samePasswords
            trigger('password')
          },
        })}
      />
      <Button
        type="submit"
        variant="flat"
        color="primary"
        className="max-w-[250px] w-full mx-auto"
        isDisabled={!!Object.keys(errors).length}
      >
        {t.submit}
      </Button>
    </form>
  )
}
