'use client'
import { Select as SelectComponent, SelectItem } from '@nextui-org/react'

type Option = {
  id: string | number
  name: string
}

type Props = {
  name: string
  label: string
  placeholder: string
  options: Option[]
}

export const Select = ({ options, name, label, placeholder }: Props) => {
  return (
    <SelectComponent label={label} placeholder={placeholder} name={name}>
      {options.map((el) => (
        <SelectItem key={el.id} value={el.id}>
          {el.name}
        </SelectItem>
      ))}
    </SelectComponent>
  )
}
