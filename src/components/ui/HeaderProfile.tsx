'use client'

import {
  Avatar,
  DropdownItem,
  DropdownTrigger,
  Dropdown,
  DropdownMenu,
  DropdownSection,
} from '@nextui-org/react'
import { ROUTES } from '@/libs/router'
import { Profile } from '@/types/api/profile'
import { logout } from '@/app/api/auth'
import { toast } from 'sonner'
import { getResponseMessage } from '@/helpers/api'
import { useRouter } from '@/navigation'
import { Locales } from '@/types/i18n'

export type Translations = {
  profile: string
  companies: string
  leads: string
  sign_out: string
}

type Props = {
  profile: Profile
  locale: Locales
  t: Translations
}

export const HeaderProfile = ({ profile, t, locale }: Props) => {
  const router = useRouter()

  const handleLogout = async () => {
    const response = await logout()
    if (response === null) return
    if (!response.success) {
      toast.error(getResponseMessage(response))
      return
    }
    router.push(ROUTES.HOME)
  }

  const menu = [
    {
      label: t.profile,
      href: ROUTES.PROFILE,
    },
    {
      label: t.companies,
      href: ROUTES.COMPANIES,
    },
    {
      label: t.leads,
      href: ROUTES.LEADS,
    },
  ]

  return (
    <Dropdown placement="bottom-end">
      <DropdownTrigger>
        <button className="flex gap-3 outline-none">
          <Avatar
            isBordered
            className="transition-transform"
            color="primary"
            size="sm"
            name={profile.name}
            src={profile.photo ? profile.photo : '/user.svg'}
          />
          <div className="flex flex-col items-start justify-center">
            <h4 className="text-[16px] leading-none text-primary-500">
              {profile.name}
            </h4>
            <h5 className="text-[12px] text-default-400">{profile.email}</h5>
          </div>
        </button>
      </DropdownTrigger>

      <DropdownMenu aria-label="Profile Actions" variant="flat">
        <DropdownSection items={menu}>
          {menu.map((el) => (
            <DropdownItem key={el.label} onClick={() => router.push(el.href)}>
              {el.label}
            </DropdownItem>
          ))}
        </DropdownSection>
        <DropdownItem key="logout" color="danger" onClick={handleLogout}>
          {t.sign_out}
        </DropdownItem>
      </DropdownMenu>
    </Dropdown>
  )
}
