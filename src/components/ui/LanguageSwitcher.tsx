'use client'

import { locales } from '@/i18n'
import { usePathname, useRouter } from '@/navigation'
import { Locales } from '@/types/i18n'
import {
  Dropdown,
  DropdownTrigger,
  DropdownMenu,
  DropdownItem,
  Selection,
} from '@nextui-org/react'
import Image from 'next/image'

type Props = {
  locale: Locales
}

export const LanguageSwitcher = ({ locale }: Props) => {
  const router = useRouter()
  const pathname = usePathname()

  const handleChange = (selection: Selection) => {
    const [selected] = Array.from(selection)
    router.replace(pathname, { locale: selected as Locales })
  }

  return (
    <Dropdown>
      <DropdownTrigger>
        <div className="flex items-end cursor-pointer">
          <span>{locale}</span>
          <Image src="/arrow.svg" alt="arrow" width={20} height={20} />
        </div>
      </DropdownTrigger>
      <DropdownMenu
        aria-label="Locales"
        variant="flat"
        disallowEmptySelection
        selectionMode="single"
        selectedKeys={new Set([locale])}
        onSelectionChange={handleChange}
      >
        {locales.map((l) => (
          <DropdownItem key={l}>{l}</DropdownItem>
        ))}
      </DropdownMenu>
    </Dropdown>
  )
}
