'use client'

import { SubmitHandler, useForm } from 'react-hook-form'
import { Button, Input } from '@nextui-org/react'
import { ROUTES } from '@/libs/router'
import { toast } from 'sonner'
import { generateCompanyRequest, getResponseMessage } from '@/helpers/api'
import { Company } from '@/types/api/companies'
import { createCompany, updateCompany } from '@/app/api/companies'
import { useRouter } from '@/navigation'

export type Translations = {
  labels: {
    companyName: string
    boardName: string
  }
  validations: {
    required: string
  }
  submit: string
}

type Props = {
  t: Translations
  company?: Company
}

export type FormValues = {
  companyName: string
  boardName: string
}

export const CompanyForm = ({ t, company }: Props) => {
  const router = useRouter()
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormValues>()

  const submit: SubmitHandler<FormValues> = async (fv) => {
    const preparedData = generateCompanyRequest(fv)
    const response = await (company
      ? updateCompany({ id: company.id, fv: preparedData })
      : createCompany(preparedData))

    if (response === null) return

    if ('success' in response) {
      toast.error(getResponseMessage(response))
      return
    }

    if (!company) {
      router.push(ROUTES.COMPANIES)
    }
  }

  return (
    <form onSubmit={handleSubmit(submit)} className="flex flex-col gap-6">
      <Input
        variant="flat"
        labelPlacement="outside"
        defaultValue={company?.name ?? ''}
        label={t.labels.companyName}
        isInvalid={!!errors.companyName?.message}
        errorMessage={errors.companyName?.message}
        color={!!errors.companyName?.message ? 'danger' : 'default'}
        {...register('companyName', {
          required: t.validations.required,
        })}
      />

      <Input
        variant="flat"
        labelPlacement="outside"
        defaultValue={company?.boards[0].name ?? ''}
        label={t.labels.boardName}
        isInvalid={!!errors.boardName?.message}
        errorMessage={errors.boardName?.message}
        color={!!errors.boardName?.message ? 'danger' : 'default'}
        {...register('boardName', {
          required: t.validations.required,
        })}
      />

      <Button
        type="submit"
        variant="flat"
        color="primary"
        className="max-w-[250px] w-full mx-auto"
        isDisabled={!!Object.keys(errors).length}
      >
        {t.submit}
      </Button>
    </form>
  )
}
